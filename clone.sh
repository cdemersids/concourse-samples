#!/bin/bash

mkdir ~/.ssh
echo "$GIT_PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 400 ~/.ssh/id_rsa
ssh-keyscan bitbucket.org >> ~/.ssh/known_hosts

git clone git@bitbucket.org:idstacks/app-fr-am.git